<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DevopsControllerTest extends WebTestCase
{
    public function testWelcome()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
    }

    public function testSomething()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/devops');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHasHeader('Content-Type');
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $json_response = $client->getResponse()->getContent();
        $this->assertJson($json_response);
        $content = json_decode($json_response, true);
        $this->assertIsArray($content);
        $this->assertEquals(1, sizeof($content));
        $this->assertEquals("thibault", $content[0]['name']);
    }
}
