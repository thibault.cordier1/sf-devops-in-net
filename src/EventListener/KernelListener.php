<?php


namespace App\EventListener;


use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Component\Stopwatch\StopwatchEvent;

class KernelListener
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Stopwatch
     */
    private $stopwatch;

    /**
     * @var StopwatchEvent[]
     */
    private $events = [];

    /**
     * KernelListener constructor.
     * @param LoggerInterface $logger
     * @param Stopwatch $stopwatch
     */
    public function __construct(LoggerInterface $logger, Stopwatch $stopwatch)
    {
        $this->logger = $logger;
        $this->stopwatch = $stopwatch;
        $this->stopwatch->openSection();

    }

    public function onKernelRequest(RequestEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }
        $this->stopwatch->start("onKernelRequest", "telemetry");
    }

    public function onKernelController(ControllerEvent $controllerEvent)
    {
        $this->logger->info("Call to Controller");
        if ($this->stopwatch->isStarted("onKernelRequest")) {
            $this->events["onKernelRequest"] = $this->stopwatch->stop("onKernelRequest");
        }
        $this->stopwatch->start("onKernelController", "telemetry");

    }

    public function onKernelException(ExceptionEvent $event)
    {
        if ($this->stopwatch->isStarted("onKernelController")) {
            $this->events["onKernelController"] = $this->stopwatch->stop("onKernelController");
        }
        $this->stopwatch->start("onKernelResponse", "telemetry");
    }

    public function onKernelResponse(ResponseEvent $responseEvent)
    {
        if ($this->stopwatch->isStarted("onKernelController")) {
            $this->events["onKernelController"] = $this->stopwatch->stop("onKernelController");
        }
        $this->stopwatch->start("onKernelResponse", "telemetry");

        $status_code = $responseEvent->getResponse()->getStatusCode();
        $this->logger->info(sprintf("status_code: %s", $status_code));
    }

    public function onKernelTerminate(TerminateEvent $terminateEvent)
    {

        if ($this->stopwatch->isStarted("onKernelResponse")) {
            $this->events["onKernelResponse"] = $this->stopwatch->stop("onKernelResponse");
        }
        $this->stopwatch->stopSection("telemetry");
        $total_duration = 0;
        foreach ($this->events as $event_name => $event) {
            $this->logger->info(sprintf("Event name: %s Memory:  %s Mb, Duration: %s ms",
                $event_name,
                $event->getMemory() / 1024 / 1024,
                $event->getDuration()
            ));
            $total_duration += $event->getDuration();
        }
        $this->logger->info(sprintf("Total duration %s ms", $total_duration));

        $sections = $this->stopwatch->getSections();
        foreach ($sections as $section) {
            $this->logger->info($section->getId());
            foreach ($section->getEvents() as $event_name => $event) {

                $this->logger->info(
                    sprintf("event_name: %s, category: %s, duration: %s ms, memory: %s Mb",
                        $event_name,
                        $event->getCategory(),
                        $event->getDuration(),
                        $event->getMemory() / 1024 / 1024
                    ));
            }

        }
    }


}