<?php

namespace App\Controller;

use App\Repository\DevopsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class DevopsController extends AbstractController
{
    /**
     * @Route("/", name="welcome")
     * @return Response
     */
    public function welcomePage()
    {
        return new Response("Hello Devops !", 200);
    }

    /**
     * @Route("/devops", name="devops")
     * @param DevopsRepository $devopsRepository
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function listDevops(DevopsRepository $devopsRepository, SerializerInterface $serializer)
    {
        $devopsList = $devopsRepository->findAll();

        return new Response($serializer->serialize($devopsList, 'json'), 200, array('Content-Type' => 'application/json'));


    }
}
