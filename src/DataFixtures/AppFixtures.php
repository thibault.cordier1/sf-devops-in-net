<?php

namespace App\DataFixtures;

use App\Entity\Devops;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $devops = new Devops();
        $devops->setName("thibault");
        $manager->persist($devops);
        $manager->flush();
    }
}
