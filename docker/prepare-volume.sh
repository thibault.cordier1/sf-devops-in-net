#!/bin/bash


if [ -z "$VOLUME_DIRECTORY" ]; then
  echo "ERROR: variables not set"
  echo ""
  echo "Env variables VOLUME_DIRECTORY, ARCHIVE_PATH, PROJECT_DIR must be defined"
  echo " - VOLUME_DIRECTORY = Mount point of the volume, example: /var/www/html"
  echo "- ARCHIVE_PATH = Path of the archive to uncompress, example /tmp/project.tar.gz"
  echo "- PROJECT_DIR = Path of the project for extraction, example: symfony"
  echo ""
  exit 1
fi

if [ ! -d "$VOLUME_DIRECTORY" ]; then
  echo "Volume mount $VOLUME_DIRECTORY doesn't exists"
  exit 1
fi

if [ ! -f "$ARCHIVE_PATH" ]; then
  echo "Archive $ARCHIVE_PATH doesn't exists"
  exit 1
fi

if [ ! -d "$VOLUME_DIRECTORY/$PROJECT_DIR" ]; then
  mkdir -p "$VOLUME_DIRECTORY/$PROJECT_DIR"
fi

if [ ! -f "$VOLUME_DIRECTORY/$PROJECT_DIR/.extraction_done" ]; then
  tar zxf "$ARCHIVE_PATH" -C "$VOLUME_DIRECTORY/$PROJECT_DIR"
  touch "$VOLUME_DIRECTORY/$PROJECT_DIR"/.extraction_done
fi

chown -R www-data:www-data "$VOLUME_DIRECTORY/$PROJECT_DIR"