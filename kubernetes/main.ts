import {Construct} from 'constructs';
import {App, Chart} from 'cdk8s';
import {
    ConfigMap,
    Deployment,
    Ingress,
    PersistentVolumeClaim,
    Quantity,
    Secret,
    Service,
    StatefulSet
} from "./imports/k8s";
import {ImagePullPolicy, Secret as ImportedSecret} from 'cdk8s-plus';
import * as fs from 'fs';
import {URL} from 'url';

export class MyChart extends Chart {
    constructor(scope: Construct, name: string) {
        super(scope, name);

        // define resources here
        const tag = process.env.TAG || 'dev';
        const fpmImageName = process.env.FPM_IMAGE_NAME;
        let ingressDomain = new URL('https://' + process.env.INGRESS_DOMAIN || 'http://localhost').host;

        const ciBranch = process.env.CI_COMMIT_REF_SLUG || 'unknown';
        if (ciBranch == 'master') {

            ingressDomain = new URL('https://' + process.env.PRODUCTION_DOMAIN || 'https://www.devops.in.net').host;
        }

        const redisSvcPort = '6379';


        const configMapNginxConf = new ConfigMap(this, 'nginx-conf', {
            data: {
                "nginx.conf": fs.readFileSync(`${__dirname}/nginx/nginx.conf`, 'utf-8'),
                "symfony.vhost": fs.readFileSync(`${__dirname}/nginx/symfony.vhost`, 'utf-8'),
            },
        })


        const sfService = new Service(this, "sf-devops-in-in-net-svc", {
            metadata: {
                annotations: {
                    "prometheus.io/scrape": 'true',
                    "prometheus.io/path": "/metrics/prometheus"
                },
                labels: {
                    "app": "sf-devops-in-net",
                    "version": tag,
                    "svc": "php",
                    "application": process.env.CI_ENVIRONMENT_SLUG || "sf-devops-in-net",
                    "release": process.env.CI_ENVIRONMENT_SLUG || "sf-devops-in-net"
                }
            },
            spec: {
                selector: {
                    "app": "sf-devops-in-net",
                    "version": tag,
                    "svc": "php"
                },
                ports: [
                    {
                        name: "nginx",
                        port: 80,
                        targetPort: 8090
                    }
                ]
            }
        });

        const redisService = new Service(this, "sf-devops-in-net-redis-svc", {
            metadata: {
                annotations: {
                    "prometheus.io/scrape": 'true'
                },
                labels: {
                    "app": "sf-devops-in-net",
                    "version": tag,
                    "svc": "redis",
                    "application": process.env.CI_ENVIRONMENT_SLUG || "sf-devops-in-net",
                    "release": process.env.CI_ENVIRONMENT_SLUG || "sf-devops-in-net"
                }
            },
            spec: {
                selector: {
                    "app": "sf-devops-in-net",
                    "version": tag,
                    "svc": "redis"
                },
                ports: [
                    {
                        name: "redis",
                        port: 6379,
                        targetPort: 6379
                    }
                ]
            }
        })

        const mysqlService = new Service(this, "sf-devops-in-net-db-svc", {
            metadata: {
                annotations: {
                    "prometheus.io/scrape": 'true'
                },
                labels: {
                    "app": "sf-devops-in-net",
                    "version": tag,
                    "svc": "mysql",
                    "application": process.env.CI_ENVIRONMENT_SLUG || "sf-devops-in-net",
                    "release": process.env.CI_ENVIRONMENT_SLUG || "sf-devops-in-net"
                }
            },
            spec: {
                selector: {
                    "app": "sf-devops-in-net",
                    "version": tag,
                    "svc": "mysql"
                },
                ports: [
                    {
                        name: "mysql",
                        port: 3306,
                        targetPort: 3306
                    }
                ]
            }
        });

        const mysqlSecret = new Secret(this, 'sf-devops-mysql-secret', {
            stringData: {
                "MYSQL_ROOT_PASSWORD": "F8bU3h7kE",
                "MYSQL_DATABASE": "test",
                "MYSQL_USER": "gitlab-runner",
                "MYSQL_PASSWORD": "29exF7YgD",
                "DATABASE_URL": "mysql://gitlab-runner:29exF7YgD@" + mysqlService.name + ":3306/test?serverVersion=5.7"
            }
        });

        const mysqlPVC = new PersistentVolumeClaim(this, 'mysql-pvc', {
            spec: {
                accessModes: ["ReadWriteOnce"],
                resources: {
                    requests: {
                        "storage": Quantity.fromString("10Gi")
                    }
                }
            }
        });

        new Deployment(this, 'sf-devops-in-net-redis-deploy', {
            metadata: {
                name: process.env.CI_ENVIRONMENT_SLUG + "-redis" || "dev-redis" ,
                labels: {
                    "app": "sf-devops-in-net",
                    "version": tag,
                    "svc": "redis",
                }
            },
            spec: {
                selector: {
                    matchLabels: {
                        "app": "sf-devops-in-net",
                        "version": tag,
                        "svc": "redis"
                    }
                },
                template: {
                    metadata: {
                        annotations: {
                            "app.gitlab.com/env": process.env.CI_ENVIRONMENT_SLUG || "unknown-env-slug",
                            "app.gitlab.com/app": process.env.CI_PROJECT_PATH_SLUG || "unknown-slug"
                        },
                        labels: {
                            "app": "sf-devops-in-net",
                            "version": tag,
                            "svc": "redis"
                        }
                    },
                    spec: {
                        containers: [
                            {
                                name: "redis",
                                image: "redis:5-alpine",
                            }
                        ]
                    }
                }
            }
        });

        new Deployment(this, 'sf-devops-in-net-deployment', {
            metadata: {
                name: process.env.CI_ENVIRONMENT_SLUG + "-sf" || "dev-sf" ,
                annotations: {
                    "app.gitlab.com/env": process.env.CI_ENVIRONMENT_SLUG || "unknown-env-slug",
                    "app.gitlab.com/app": process.env.CI_PROJECT_PATH_SLUG || "unknown-slug"
                },
                labels: {
                    "app": "sf-devops-in-net",
                    "version": tag,
                    "svc": "php"
                }
            },
            spec: {
                selector: {
                    matchLabels: {
                        "app": "sf-devops-in-net",
                        "version": tag,
                        "svc": "php"
                    }
                },
                replicas: 2,
                template: {
                    metadata: {
                        annotations: {
                            "date": Date.now().toString(),
                            "commit": process.env.CI_COMMIT_SHA || "none",
                            "author": process.env.GITLAB_USER_EMAIL || "unknown",
                            "app.gitlab.com/env": process.env.CI_ENVIRONMENT_SLUG || "unknown-env-slug",
                            "app.gitlab.com/app": process.env.CI_PROJECT_PATH_SLUG || "unknown-slug"
                        },

                        labels: {
                            "app": "sf-devops-in-net",
                            "version": tag,
                            "svc": "php"
                        }
                    },
                    spec: {

                        imagePullSecrets: [
                            ImportedSecret.fromSecretName("scwregcred")
                        ],
                        initContainers: [
                            {
                                image: fpmImageName + ':' + tag,
                                imagePullPolicy: ImagePullPolicy.ALWAYS,
                                command: ["/usr/local/bin/prepare-volume.sh"],
                                name: "php-prepare-volume",
                                env: [
                                    {
                                        name: "VOLUME_DIRECTORY",
                                        value: "/var/www/html"
                                    },
                                    {
                                        name: "PROJECT_DIR",
                                        value: "symfony"
                                    },
                                    {
                                        name: "ARCHIVE_PATH",
                                        value: "/opt/project.tar.gz"
                                    },
                                    {
                                        name: "APP_ENV",
                                        value: "prod"
                                    },
                                    {
                                        name: "DATABASE_URL",
                                        valueFrom: {
                                            secretKeyRef: {
                                                name: mysqlSecret.name,
                                                key: "DATABASE_URL"
                                            }

                                        }
                                    },
                                    {
                                        name: "REDIS_HOST",
                                        value: redisService.name,
                                    },
                                    {
                                        name: "REDIS_PORT",
                                        value: redisSvcPort
                                    },
                                    {
                                        name: "REDIS_DATABASE",
                                        value: "2"
                                    }
                                ],

                                volumeMounts: [
                                    {
                                        name: "application-data",
                                        mountPath: "/var/www/html"

                                    }
                                ]
                            }
                        ],
                        containers: [
                            {
                                name: "nginx",
                                image: "nginx:latest",
                                volumeMounts: [
                                    {
                                        mountPath: '/etc/nginx/nginx.conf',
                                        name: 'nginx-conf',
                                        subPath: 'nginx.conf'
                                    },
                                    {
                                        mountPath: '/etc/nginx/sites-enabled/symfony.vhost',
                                        name: 'nginx-conf',
                                        subPath: 'symfony.vhost'
                                    },
                                    {
                                        name: "application-data",
                                        mountPath: "/var/www/html"

                                    },

                                ]
                            },
                            {
                                image: fpmImageName + ':' + tag,
                                name: "php-fpm",
                                imagePullPolicy: ImagePullPolicy.ALWAYS,
                                env: [
                                    {
                                        name: "VOLUME_DIRECTORY",
                                        value: "/var/www/html"
                                    },
                                    {
                                        name: "PROJECT_DIR",
                                        value: "symfony"
                                    },
                                    {
                                        name: "ARCHIVE_PATH",
                                        value: "/opt/project.tar.gz"
                                    },
                                    {
                                        name: "APP_ENV",
                                        value: "prod"
                                    },
                                    {
                                        name: "REDIS_URL",
                                        value: 'redis://' + redisService.name + ":" + redisSvcPort + "/1"
                                    },
                                    {
                                        name: "DATABASE_URL",
                                        valueFrom: {
                                            secretKeyRef: {
                                                name: mysqlSecret.name,
                                                key: "DATABASE_URL"
                                            }

                                        }
                                    },
                                    {
                                        name: "REDIS_HOST",
                                        value: redisService.name,
                                    },
                                    {
                                        name: "REDIS_PORT",
                                        value: redisSvcPort
                                    },
                                    {
                                        name: "REDIS_DATABASE",
                                        value: "2"
                                    }
                                ],

                                volumeMounts: [
                                    {
                                        name: "application-data",
                                        mountPath: "/var/www/html"

                                    }
                                ]
                            }
                        ],
                        volumes: [
                            {
                                name: "application-data",
                                emptyDir: {}
                            },
                            {
                                name: "nginx-conf",
                                configMap: {
                                    name: configMapNginxConf.name
                                }
                            }

                        ]
                    }
                }
            }
        })

        new StatefulSet(this, 'sf-devops-in-net-db-stateful', {
            metadata: {
                labels: {
                    "app": "sf-devops-in-net",
                    "version": tag,
                    "svc": "mysql"
                }
            },
            spec: {
                serviceName: mysqlService.name,
                selector: {
                    matchLabels: {
                        "app": "sf-devops-in-net",
                        "version": tag,
                        "svc": "mysql"
                    }
                },
                replicas: 1,
                template: {
                    metadata: {
                        name: "sf-devops-in-net-db",
                        annotations: {
                            "app.gitlab.com/env": process.env.CI_ENVIRONMENT_SLUG || "unknown-env-slug",
                            "app.gitlab.com/app": process.env.CI_PROJECT_PATH_SLUG || "unknown-slug"
                        },
                        labels: {
                            "app": "sf-devops-in-net",
                            "version": tag,
                            "svc": "mysql",

                        }
                    },
                    spec: {
                        containers: [
                            {
                                resources: {
                                    requests: {
                                        "cpu": Quantity.fromString("250m"),
                                        "memory": Quantity.fromString("128Mi")
                                    },
                                    limits: {
                                        "cpu": Quantity.fromString("500m"),
                                        "memory": Quantity.fromString("256Mi")
                                    }
                                },
                                readinessProbe: {
                                    tcpSocket: {
                                        port: 3306
                                    },
                                    initialDelaySeconds: 20,
                                    periodSeconds: 5
                                },
                                livenessProbe: {
                                    tcpSocket: {
                                        port: 3306
                                    },
                                    initialDelaySeconds: 30,
                                    periodSeconds: 5
                                },
                                image: 'mysql:5.7',
                                name: 'mysql',
                                args: ["--ignore-db-dir=lost+found"],
                                envFrom: [
                                    {
                                        secretRef: {
                                            name: mysqlSecret.name
                                        }
                                    }
                                ],
                                volumeMounts: [
                                    {
                                        name: "mysql-data",
                                        mountPath: "/var/lib/mysql"
                                    }
                                ]

                            }
                        ],
                        volumes: [
                            {
                                name: "mysql-data",
                                persistentVolumeClaim: {
                                    claimName: mysqlPVC.name
                                }
                            }
                        ]
                    },

                },
            }
        });

        new Ingress(this, 'sf-devops-in-net-ingress', {
            metadata: {
                annotations: {
                    "prometheus.io/scrape": 'true',
                    "kubernetes.io/tls-acme": "true",
                    "kubernetes.io/ingress.class": "nginx",

                }
            },
            spec: {
                tls: [
                    {
                        hosts: [
                            ingressDomain
                        ],
                        secretName: "sf-devops-in-net-tls"
                    }
                ],
                rules: [
                    {
                        host: ingressDomain,
                        http: {
                            paths: [
                                {
                                    path: "/",
                                    backend: {
                                        serviceName: sfService.name,
                                        servicePort: 80
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        })


    }
}

const app = new App();
new MyChart(app, 'kubernetes');
app.synth();
